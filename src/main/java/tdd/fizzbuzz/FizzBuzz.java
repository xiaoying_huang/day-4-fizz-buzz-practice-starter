package tdd.fizzbuzz;

public class FizzBuzz {
    public static final String BUZZ = "Buzz";
    public static final String FIZZ_BUZZ = "FizzBuzz";
    public static final String FIZZ_WHIZZ = "FizzWhizz";
    public static final String BUZZ_WHIZZ = "BuzzWhizz";
    private final String  FIZZ = "Fizz";
    public String countOff(int number) {
        boolean isMultiple3 = number % 3 == 0;
        boolean isMultiple5 = number % 5 == 0;
        boolean isMultiple7 = number % 7 == 0;
        if(isMultiple3 && isMultiple5 && isMultiple7){
            return "FizzBuzzWhizz";
        }
        if(isMultiple5 &&isMultiple7){
            return BUZZ_WHIZZ;
        }
        if(isMultiple5 && isMultiple7){
            return FIZZ_WHIZZ;
        }
        if(isMultiple3 && isMultiple5){
            return FIZZ_BUZZ;
        }
        if(isMultiple3){
            return FIZZ;
        }

        if(isMultiple5){
            return BUZZ;
        }

        return String.valueOf(number);
    }
}
