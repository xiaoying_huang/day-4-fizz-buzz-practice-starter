package tdd.fizzbuzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {
    @Test
    void should_return_normal_number_when_countOff_given_normal_number() {
        //given
        int normalNumber = 1;
        FizzBuzz fizzBuzz = new FizzBuzz();

        //when
        String result = fizzBuzz.countOff(normalNumber);

        //then
        Assertions.assertEquals("1", result);
    }

    @Test
    void should_return_Fizz_String_when_countOff_given_multiple_of_3() {
        //given
        int number = 3;
        FizzBuzz fizzBuzz = new FizzBuzz();

        //when
        String result = fizzBuzz.countOff(number);

        //then
        Assertions.assertEquals("Fizz", result);
    }

    @Test
    void should_return_Buzz_String_when_countOff_given_multiple_of_5() {
        int number = 5;
        FizzBuzz fizzBuzz = new FizzBuzz();

        //when
        String result = fizzBuzz.countOff(number);

        //then
        Assertions.assertEquals("Buzz", result);
    }

    @Test
    void should_return_FizzBuzz_String_when_countOff_given_multiple_of_3_and_5() {
        int number = 15;
        FizzBuzz fizzBuzz = new FizzBuzz();

        //when
        String result = fizzBuzz.countOff(number);

        //then
        Assertions.assertEquals("FizzBuzz", result);
    }

    @Test
    void should_return_FizzWhizz_when_countOff_given_multiple_of_3_and_7() {
        int number = 21;
        FizzBuzz fizzBuzz = new FizzBuzz();

        //when
        String result = fizzBuzz.countOff(number);

        //then
        Assertions.assertEquals("FizzWhizz", result);
    }

    @Test
    void should_return_BuzzWhizz_when_countOff_given_multiple_of_5_and_7() {
        int number = 35;
        FizzBuzz fizzBuzz = new FizzBuzz();

        //when
        String result = fizzBuzz.countOff(number);

        //then
        Assertions.assertEquals("BuzzWhizz", result);
    }

    @Test
    void should_return_FizzBuzzWhizz_when_countOff_given_multiple_of_3_and_5_and_7() {
        int number = 105;
        FizzBuzz fizzBuzz = new FizzBuzz();

        //when
        String result = fizzBuzz.countOff(number);

        //then
        Assertions.assertEquals("FizzBuzzWhizz", result);
    }
}
